<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        return view('todo_show')->with('todoArr',Todo::all());
    }


    public function store(Request $request)
    {
        $res=new Todo;
        $res->name=$request->input('name');
        $res->save();
        $request->session()->flash('msg','Data submitted');
        return redirect('todo_show');
    }


    public function show()
    {
        return view('todo_show')->with('todoArr',Todo::all());
    }


    public function edit()
    {
        return view('todo_edit')->with('todoArr',Todo::find($id));
    }


    public function update(Request $request)
    {
      print_r($request->input());
    }


    public function destroy($id)
    {
        Todo::destroy(array('id',$id));
        redirect('todo_show');
    }
}
