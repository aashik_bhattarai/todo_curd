<doctype html>
    <<html lang="en">
    <head>
        <meta charset=utf-8">
        <title>Todo</title>
    </head>
    <body>
    <div>
        <style>

            #customers td, #customers th {
                border: 1px solid #ddd;
                padding: 8px;
            }

            #customers tr:nth-child(even){background-color: #f2f2f2;}

            #customers tr:hover {background-color: #ddd;}

            #customers th {
                padding-top: 12px;
                padding-bottom: 12px;
                text-align: left;
                background-color: #4CAF50;
                color: white;
            }
        </style>

        <a href="todo_show">back</a><br><br>
        <form method="post" action="{{route('todo.update',[$todoArr->name])}}">
        @csrf
        <table>
            <tr>
                <td>Name</td>
                <td><input type="textname" name="name" required value="{{$todoArr->name}}"></td>

            </tr>
            @foreach($todoArr as $todo)

                <tr>
                    <td><{{$todo->id}}/td>
                    <td>{{$todo->name}}</td>
                    <td>{{$todo->created_at}}</td>
                    <td>
                        <a href="todo_delete/{{$todo->id}}">Delete</a></td>
                    <a href="todo_delete/{{$todo->id}}">Edit</a></td>
                </tr>
            @endforeach
        </table>
    </div>
    </body>
    </html>
</doctype>
